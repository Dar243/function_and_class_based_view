from typing import Any
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.views import View
from .forms import *
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic.list import ListView
from django.views.generic.edit import *

# Create your views here.

# Function Based View:
# def add_show(req):
#     if req.method == 'POST':
#         fm  = StudentRegistration(req.POST)
#         if fm.is_valid():
#             fm.save()
#             fm  = StudentRegistration()
#     else:
#         fm  = StudentRegistration()
#     stud = User.objects.all()
#     return render(req, 'school/add_and_show.html', {'form': fm, 'stu': stud})


# def delete_data(req, id):
#     if req.method == 'POST':
#         pi = User.objects.get(pk=id)
#         pi.delete()
#         return HttpResponseRedirect('/')
    

# def update_data(req, id):
#     if req.method == 'POST':
#         pi = User.objects.get(pk = id)
#         fm = StudentRegistration(req.POST, instance=pi)
#         if fm.is_valid():
#             fm.save()
#     else:
#         pi = User.objects.get(pk = id)
#         fm = StudentRegistration(instance=pi)
#     return render(req, 'school/update_student.html', {'form': fm})



# Class Based View:

class UserAddShowView(TemplateView, ListView):
    template_name = 'school/add_and_show.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        fm = StudentRegistration()
        stud = User.objects.all()
        context = {'stu': stud, 'form': fm}
        return context
    
    def post(self, req):
        if req.method == 'GET':
            fm  = StudentRegistration()
            return render(req, 'school/add_and_show.html', {'form': fm})
        fm  = StudentRegistration(req.POST)
        if fm.is_valid():
            fm.save()
        return HttpResponseRedirect('/')


# class UserListView(ListView):
#     model = User
#     template_name = 'school/add_and_show.html'
#     context_object_name = 'stu'

    # def post(self, req):
    #     fm  = StudentRegistration(req.POST)
    #     if fm.is_valid():
    #         fm.save()
    #     return HttpResponseRedirect('/')    

class StudentRegistrationFormView(CreateView):
    # template_name = 'school/add_and_show.html'
    # form_class = StudentRegistration
    # success_url = '/'
    model = User
    fields = '__all__'
    success_url = '/'
    template_name = 'school/add_and_show.html'



class UserDeleteView(RedirectView):
    url = '/'

    def get_redirect_url(self, *args, **kwargs):
        del_id = kwargs['id']
        User.objects.get(pk = del_id).delete()

        return super().get_redirect_url(*args, **kwargs)
    


class UserUpdateView(View):
    def get(s, req, id):
        pi =  User.objects.get(pk = id)
        fm = StudentRegistration(instance=pi)
        return render(req, 'school/update_student.html', {'form': fm})
    
    def post(s, req, id):
        pi = User.objects.get(pk = id)
        fm = StudentRegistration(req.POST, instance=pi)
        if fm.is_valid():
            fm.save()
        return HttpResponseRedirect('/')
    