from django import forms
from .models import *

# class ContactForm(forms.Form):
#     name = forms.CharField(max_length=70)


class StudentRegistration(forms.ModelForm):
    class Meta:
        model= User
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(render_value=True, attrs={'class': 'form-control'}),
        }
