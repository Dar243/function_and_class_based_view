"""
URL configuration for CBV project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from school import views as v

urlpatterns = [
    path('admin/', admin.site.urls),
    # path("", v.UserAddShowView.as_view(), name='add_and_show'),
    path("", v.UserAddShowView.as_view(), name='add_and_show'),
    path("delete_data/<int:id>", v.UserDeleteView.as_view(), name='delete_data'),
    path("<int:id>/", v.UserUpdateView.as_view(), name='update_data'),
    # path('homeFun/', v.homeFun, name='homeFun'),
    # path('homeCBV/', v.HomeClassView.as_view(), name='homeCBV'),
    # path('aboutFun/', v.aboutFun, name='aboutFun'),
    # path('aboutCBV/', v.AboutClassView.as_view(), name='aboutCBV'),
    # path('contactFun/', v.contactFormFun, name='contactFun'),
    # path('contactCBV/', v.ContactFormClass.as_view(), name='contactCBV'),
    # path('', v.HomeTemplateView.as_view(extra_context = {'course': 'Python'}), name='home'),
    # path('home/', v.RedirectView.as_view(url='/'), name='home'),
    # path('index/', v.RedirectView.as_view(pattern_name='home'), name='index'),
    # path('home/<int:pk>/', v.MyRedirectView.as_view(pattern_name='home'), name='my'),
    # path('<int:pk>/', v.TemplateView.as_view(template_name='school/home.html'), name='myindex'),
    # path('home/<slug:post>/', v.TemplateView.as_view(template_name='school/home.html'), name='mindex'),
]